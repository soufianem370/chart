{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to
this (by the DNS naming spec). Supports the legacy fullnameOverride setting
as well as the global.name setting.
*/}}
{{- define "python.fullname" -}}
{{- printf .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
